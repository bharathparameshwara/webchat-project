const express = require('express')
const request = require('request');
const app = express()
var port = process.env.PORT || 8080;
const server = require('http').createServer(app);
const io = require('socket.io')(server);

app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
server.listen(port, () =>
    console.log(`HTTP Serer with Express.js is listening on port: ${port}`))
app.get('/',(require,res) => {
    res.sendFile(__dirname + '/index.html');
})

io.on('connection', (socketclient) => {
    // console.log('A new client is connected!');

    

    socketclient.on("login", (username,password) => {
        checkLogin(username,password,(authenticated) =>{
            if(authenticated){
                socketclient.authenticated = true;
                socketclient.username = username;
                socketclient.emit("authenticated");
                console.log(`User is authenticated: ${username}`)
                var onlineClients = getNumberOfAuthClients();
                var welcomemessage = `${socketclient.id} is connected! Number of connected client: ${onlineClients}`;
                console.log(welcomemessage);
                broadcastToAuthClients("online",welcomemessage);
            } else{
                socketclient.emit("authentication-failed");
                console.log(`Login failed for user: ${username}`)
            }
        })
    })

socketclient.on("register", (username,password,fullname,email) => {
        registerUser(username,password,fullname,email,(registered,duplicate) =>{
            if(registered){
                socketclient.authenticated = true;
                socketclient.username = username;
                socketclient.emit("registered");
                console.log(`User is registered: ${username}`)
                var onlineClients = getNumberOfAuthClients();
                var welcomemessage = `${socketclient.id} is connected! Number of connected client: ${onlineClients}`;
                console.log(welcomemessage);
                broadcastToAuthClients("online",welcomemessage);
            } else if(duplicate){
                socketclient.emit("registration-duplicate");
                console.log(`User already present: ${username}`)
            } else{
                socketclient.emit("registration-failed");
                console.log(`registration failed for user: ${username}`)
            }
        })
    })


    socketclient.on("message", (data) => {
        console.log('Data from a client: ' + data);
        // io.emit("message", `${socketclient.id} says: ${data}`);
        broadcastToAuthClients("message", `${socketclient.id} says: ${data}`);
    });
    socketclient.on("typing", () => {
    console.log('Someone is typing...');

    broadcastToAuthClients("typing", `${socketclient.id} is typing...`);
    });

    socketclient.on("disconnect", () => {
    var onlineClients = getNumberOfAuthClients();
    var byemessage = `${socketclient.id} is disconnected! Number of connected client: ${onlineClients}`;
    console.log(byemessage);
    broadcastToAuthClients("online", byemessage);

    });

});

function broadcastToAuthClients(event, message){
    var sockets = io.sockets.sockets;
    for(var socketid in sockets){
        const client = sockets[socketid];
        if(client && client.authenticated){
            client.emit(event,message);
        }
    }

}

function getNumberOfAuthClients(){
    var noOfClients =0;
    var sockets = io.sockets.sockets;
    for(var socketid in sockets){
        const client = sockets[socketid];
        if(client && client.authenticated){
           noOfClients +=1 ;
        }
    }
    return noOfClients;
}

function checkLogin(username,password,callback){

    console.log('login check : ' + username);

    request
  .get('https://cca-parameshwarab1-login.herokuapp.com/authenticate?username=' + username + '&password=' + password, function(error, response, body) {
    console.log(response.statusCode) 
    console.log("Body: "+ body);
    if(body=="successful" ){
        return callback(true);
    } else{
        callback(false);
    }
    }); 


}

function registerUser(username,password,fullname,email,callback){

    console.log('login check : ' + username);
    request.post({url:'https://cca-parameshwarab1-signup.herokuapp.com/newuser', json: {username:username,password:password,fullname:fullname,email:email}}, function(err,httpResponse,body){
    if (err) { return console.log(err); }
    console.log(body.url);
    console.log(body);
    if(body=="successful" ){
        callback(true,false);
    } else if(body=="duplicate" ){
        callback(false,true);
    } else{
        callback(false,false);
    }
    });    
}